<!--
vista formulario le pondrás un botón para ver la lista de registros.
vista lista (en donde se mostrará tus registros creados) le pondrás un botón para crear un nuevo registro (vista formulario).
archivo de conexión a base de datos pdo
archivo de insert pdo
archivo de read pdo

consejos:
Usa inclusiones (include_once y require_once)
Usa el control de versiones (esto para volver si existe alguna incidencia grave)
Usa el paradigma orientado a objetos
-->

<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<title>PDO drivers</title>
	</head>
	<body>
		<h3>Drivers disponibles para PDO</h3>
		<pre>
			<?php
				print_r(PDO::getAvailableDrivers());
			?>
		</pre>
		<ul>
			<li><a href='insert.php' class="btn btn-success">Insertar datos</a></li>
			<li><a href='select.php' class="btn btn-success">Consultar los datos</a></li>
			<li><a href='transaccion.php' class="btn btn-success">Transacciones</a></li>
		</ul>
	</body>
</html>
