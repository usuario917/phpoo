<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<title>Transacciones</title>
	</head>
	<body>
		<h3>Transacciones con PDO</h3>
		<p>Transacci&oacute;n exitosa (commit)</p>
		<?php
		try {
			require_once('./conn.php');
			$dbh->beginTransaction();
			$dbh->query('INSERT INTO rector (rector_id) VALUES ("100"),("101"),("102"),("103")');
			echo "Se insertaron los rectores 100, 101, 102, 103 <br />";
			$dbh->query("DELETE FROM rector WHERE rector_id > 100");
			echo "Se borraron los rectores mayores a 100 <br />";
			$dbh->query("UPDATE rector set rec_nombre = CONCAT('Rector ', rector_id) WHERE rector_id >= 100");
			echo "Se actualizo el nombre del rector con id mayor igual a 100 <br />";
			$dbh->commit();
			echo "Se ha realizado las operaciones en rector";
			?>
			<hr /><p>Transacci&oacute;n que no guarda (rollback)</p>
			<?php
			// Tambien se puede hacer transacciones con prepare, con bindValue o bindParam y execute
			$dbh->beginTransaction();
			// Insertamos datos
			$sql = "INSERT INTO rector(rector_id, rec_nombre, cam_director) VALUES (?, ?, ?)";
			// Datos 1: Parámetros posicion
			$stmt = $dbh->prepare($sql);
			$id = 41;
			$nombre = "Tony Stark";
			$director = "Facultad de Ingeniería";
			$stmt->bindParam(1, $id);
			$stmt->bindParam(2, $nombre);
			$stmt->bindParam(3, $director);
			echo ($stmt->execute()) ? "Se agrego a $nombre" : '';
			echo "<br />";
		
			// Datos 2: Parametros nombrados
			$sql = "INSERT INTO rector(rector_id, rec_nombre, cam_director) VALUES (:id, :nombre, :director)";
			$stmt = $dbh->prepare($sql);
			$id = 42;
			$nombre = "Bruce Banner";
			$director = "Facultad de Ciencias";
			$stmt->bindParam(":id", $id);
			$stmt->bindParam(":director", $director);
			$stmt->bindParam(":nombre", $nombre);
			echo ($stmt->execute()) ? "Se agrego a $nombre" : '';
			echo "<br />";
		
			// Datos 3: Modo Lazy
			$stmt = $dbh->prepare($sql);
			echo($stmt->execute([':id'=> '43', ':nombre'=>'Steve Rogers', ':director'=>'Facultad de Derecho'])) ? 'Se agrego a Steve Rogers' : '';
			echo "<br />";

			// Actualizacion
			$dbh->query("UPDATE rector set rector_id = 1"); // actualizaqr todos los rector_id a 1
			echo "Se actualizo la tabla rector rector_id = 1 <br />";
			// Borramos toda la tabla
			$dbh->query("DELETE FROM rector"); // descomentar para borrar toda la tabla
			echo "Se borro toda la informacion de la tabla rector <br />";
			// Hacemos que no guarde la transaccion
			$dbh->rollback();
			echo "Se hizo el rollback <br />";
		} catch (Exception $e) {
			// Cualquier error lo imprimimos
			$dbh->rollback();
			echo $e->getMessage();
		} finally {
			// Cerramos la conexion a la base
			$dbh = null;
		}
		?>
		<ul>
			<li><a href='index.php' class="btn btn-success">Index</a></li>
			<li><a href='insert.php' class="btn btn-success">Insertar datos</a></li>
			<li><a href='select.php' class="btn btn-success">Consultar los datos</a></li>
		</ul>
	</body>
</html>
