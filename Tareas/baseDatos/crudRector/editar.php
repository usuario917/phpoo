<?php
	$msj = null;
	if (isset($_REQUEST['rec']) && !empty($_REQUEST['rec'])) {
		try {
			require_once('../conn.php');
			if (isset($_POST['enviar']) && !empty($_POST['enviar'])) {
				$sql = "UPDATE rector SET rec_nombre = :nom, cam_director = :dir WHERE rector_id = :rec";
				$stmt = $dbh->prepare($sql);
				$stmt->bindValue('nom', $_REQUEST['rec_nombre']);
				$stmt->bindValue('dir', $_REQUEST['cam_director']);
				$stmt->bindValue('rec', $_REQUEST['rec']);
				$stmt->execute();
			}
			$sql = "SELECT * FROM rector WHERE rector_id = :rec";
			$stmt = $dbh->prepare($sql);
			$stmt->bindValue('rec', $_REQUEST['rec']);
			$stmt->execute();
			$rector = $stmt->fetch(PDO::FETCH_OBJ);
		} catch (Exception $e) {
			// Cualquier error lo imprimimos
			$msj = $e->getMessage();
		} finally {
			// Cerramos la conexion a la base
			$dbh = null;
			if (empty($msj) && isset($_POST['enviar'])) {
				header("Location: ./index.php");
				exit();
			}
		}
	}
?>
<html>
	<head>
		<title>CRUD rector - Editar</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
		<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
		<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
	</head>
	<body>
		<div class="container">
			<div class="columns">
				<div class="column col-2"></div>
				<div class="column col-8">
					<?= $msj; ?>
					<h3>Editar Rector</h3>
					<form action='editar.php?rec=<?= $_REQUEST['rec'] ?>' method="POST">
					<div class="form-group">
						<label class="form-label" for="input-id">ID</label>
						<input name="rector_id" class="form-input" type="number" id="input-id" value="<?= $rector->rector_id; ?>" readonly disabled/>
					</div>
					<div class="form-group">
						<label class="form-label" for="input-nom">Nombre</label>
						<input name="rec_nombre" class="form-input" type="text" id="input-nom" placeholder="Nombre" maxlength="25"  value="<?= $rector->rec_nombre; ?>" />
					</div>
					<div class="form-group">
						<label class="form-label" for="input-dir">Director</label>
						<input name="cam_director" class="form-input" type="text" id="input-dir" placeholder="Director" maxlength="50" value="<?= $rector->cam_director; ?>" />
					</div>
					<input type="submit" name="enviar" class="btn btn-primary" value="Enviar" /> &nbsp;&nbsp;&nbsp;&nbsp;
					<input type="reset" class="btn" value="Cancelar" />
					</form>
				</div>
			</div>
		</div>
	</body>
</html>
