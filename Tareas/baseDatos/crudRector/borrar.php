<?php
	$msj = null;
	if (isset($_REQUEST['rec']) && !empty($_REQUEST['rec'])) {
		try {
			$idBorrar = $_REQUEST['rec'];
			// Preparamos la conexion a la base de datos
			require_once('../conn.php');
			$sql = "DELETE FROM rector WHERE rector_id = :rec";
			$stmt = $dbh->prepare($sql);
			$stmt->bindValue('rec', $idBorrar);
			var_dump($stmt->execute());
		} catch (Exception $e) {
			// Cualquier error lo imprimimos
			$msj = $e->getMessage();
			echo $msj;
			exit;
		} finally {
			// Cerramos la conexion a la base
			$dbh = null;
			if (empty($msj)) {
				header("Location: ./index.php");
				exit();
			}
		}
	}
