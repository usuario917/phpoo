<html>
	<head>
		<title>CRUD rector - Lista</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
		<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
		<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
	</head>
	<body>
	<div class="container">
		<div class="columns">
		<div class="column col-2"></div>
		<div class="column col-8">
		<h3>CRUD rector - Lista</h3>
		<a href="agregar.php"><i class="icon icon-2x icon-people"></i> Agregar</a>
		<table class="table table-striped table-hover">
			<thead>
				<tr>
					<th>Id</th>
					<th>Nombre</th>
					<th>Director de</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<?php
					require_once('../conn.php');
					try {
						// FETCH_OBJ
						$stmt = $dbh->prepare("SELECT * FROM rector");
						$stmt->execute();
						$result = $stmt->fetchAll(PDO::FETCH_OBJ);
						if (!empty($result)) {
							foreach($result as $row) {
								/* forma de imprimir o guardar multiples lineas
								<<<NOMBRE
								cadena multile linea y escapa variables
								NOMBRE;
								*/
								echo '<<<EOL
									<tr>
										<td>{$row->rector_id}</td>
										<td>{$row->rec_nombre}</td>
										<td>{$row->cam_director}</td>
										<td><a href="editar.php?rec={$row->rector_id}"><i class="icon icon-2x icon-edit"></i></a>&nbsp;&nbsp;
											<a href="borrar.php?rec={$row->rector_id}"><i class="icon icon-2x icon-delete"></i></a>
										</td>
									</tr>
								EOL';
							}
						} else {
							echo "<tr><td colspan='4'>No hay datos para mostrar</td></tr>";
						}
					} catch (Exception $e) {
						// Cualquier error lo imprimimos
						echo $e->getMessage();
					} finally {
						// Cerramos la conexion a la base
						$dbh = null;
					}
				?>
			</tbody>
			</table>
		</div>
		</div>
	</div>
	</body>
</html>

