<?php
	class Rector {
		public $rector_id;
		public $rec_nombre;
		public $cam_director;
		public function __construct($id, $nombre, $director) {
			$this->rector_id = $id;
			$this->rec_nombre = $nombre;
			$this->cam_director = $director;
		}
	}
	$msj = null;
	if (isset($_POST['enviar']) && !empty($_POST['enviar'])) {
		try {
			//Datos 4: modo clase
			$rector = new Rector($_POST['rector_id'], $_POST['rec_nombre'], $_POST['cam_director']);
			// Preparamos la conexion a la base de datos
			require_once('../conn.php');
			$sql = "INSERT INTO rector(rector_id, rec_nombre, cam_director) VALUES (:rector_id, :rec_nombre, :cam_director)";
			$stmt = $dbh->prepare($sql);
			$stmt->execute((array) $rector);
		} catch (Exception $e) {
			// Cualquier error lo imprimimos
			$msj = $e->getMessage();
		} finally {
			// Cerramos la conexion a la base
			$dbh = null;
			if (empty($msj)) {
				header("Location: ./index.php");
				exit();
			}
		}
	}
?>
<html>
	<head>
		<title>CRUD rector - Agregar</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre.min.css">
		<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-exp.min.css">
		<link rel="stylesheet" href="https://unpkg.com/spectre.css/dist/spectre-icons.min.css">
	</head>
	<body>
	<div class="container">
		<div class="columns">
			<div class="column col-2"></div>
			<div class="column col-8">
				<?= $msj; ?>
				<h3>Agregar Rector</h3>
				<form action='agregar.php' method="POST">
					<div class="form-group">
						<label class="form-label" for="input-id">ID</label>
						<input name="rector_id" class="form-input" type="number" id="input-id" placeholder="ID"  maxlength="5" pattern="[0-9]{1-5}" />
					</div>
					<div class="form-group">
						<label class="form-label" for="input-nom">Nombre</label>
						<input name="rec_nombre" class="form-input" type="text" id="input-nom" placeholder="Nombre" maxlength="25" />
					</div>
					<div class="form-group">
						<label class="form-label" for="input-dir">Director</label>
						<input name="cam_director" class="form-input" type="text" id="input-dir" placeholder="Director" maxlength="50" />
					</div>
					<input type="submit" name="enviar" class="btn btn-primary" value="Enviar" /> &nbsp;&nbsp;&nbsp;&nbsp;
					<input type="reset" class="btn" value="Cancelar" />
				</form>
			</div>
		</div>
	</div>
	</body>
</html>
