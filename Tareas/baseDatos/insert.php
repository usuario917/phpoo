<html>
	<head>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
		<title>Inserci&oacute;n de datos</title>
	</head>
	<body>
		<h3>Insertar datos</h3>
		<?php
			class Rector {
				public $id;
				public $nombre;
				public $director;
				public function __construct($id, $nombre, $director) {
					$this->id = $id;
					$this->nombre = $nombre;
					$this->director = $director;
				}
			}
			try {
				// Preparamos la conexion a la base de datos
				require_once('./conn.php');
				// Insertamos datos
				$sql = "INSERT INTO rector(rector_id, rec_nombre, cam_director) VALUES (?, ?, ?)";
				// Datos 1: Parámetros posicion
				$stmt = $dbh->prepare($sql);
				$id = 1;
				$nombre = "Tony Stark";
				$director = "Facultad de Ingeniería";
				$stmt->bindParam(1, $id);
				$stmt->bindParam(2, $nombre);
				$stmt->bindParam(3, $director);
				echo ($stmt->execute()) ? "Se agrego a $nombre" : '';
				echo "<br />";

				// Datos 2: Parametros nombrados
				$sql = "INSERT INTO rector(rector_id, rec_nombre, cam_director) VALUES (:id, :nombre, :director)";
				$stmt = $dbh->prepare($sql);
				$id = 2;
				$nombre = "Bruce Banner";
				$director = "Facultad de Ciencias";
				$stmt->bindParam(":id", $id);
				$stmt->bindParam(":director", $director);
				$stmt->bindParam(":nombre", $nombre);
				echo ($stmt->execute()) ? "Se agrego a $nombre" : '';
				echo "<br />";

				// Datos 3: Modo Lazy
				$stmt = $dbh->prepare($sql);
				echo($stmt->execute([':id'=> '3', ':nombre'=>'Steve Rogers', ':director'=>'Facultad de Derecho'])) ? 'Se agrego a Steve Rogers' : '';
				echo "<br />";

				//Datos 4: modo clase
				$rector = new Rector("4", "Natasha Romanoff", "Facultad de Ciencias Políticas");
				$stmt = $dbh->prepare($sql);
				echo($stmt->execute((array) $rector))? "Se agrego a {$rector->nombre}" : '';
				echo "<br />";
			} catch (Exception $e) {
				// Cualquier error lo imprimimos
				echo $e->getMessage();
			} finally {
				// Cerramos la conexion a la base
				$dbh = null;
			}
		?>
		<ul>
			<li><a href='index.php' class="btn btn-success">Index</a></li>
			<li><a href='select.php' class="btn btn-success">Consultar los datos</a></li>
			<li><a href='transaccion.php' class="btn btn-success">Transacciones</a></li>
		</ul>
	</body>
</html>
