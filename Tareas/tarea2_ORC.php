<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>tarea2_ORC.php</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
		<script src="main.js"></script>
	</head>
	<body>
		<?php
			//Investigar que hacen las siguientes funciones y un ejemplo sencillo de cada una de ellas

			//1. substr: Devuelve la porción de la cadena especificada por los parámetros de inicio y longitud.
			//Descripción y sintaxis:  substr ( string $string , int $start [, int $length ] ) : string
			echo "<h4>1. substr</h4>";
			echo substr("abcdef", -1)."<br>"; // devuelve "f"
			echo substr("abcdef", -2)."<br>"; // devuelve "ef"
			echo substr("abcdef", -3, 1)."<br>"; // devuelve "d"
			echo substr("abcdef", 0, -1)."<br>"; // devuelve "abcde"
			echo substr("abcdef", 2, -1)."<br>"; // devuelve "cde"
			echo substr("abcdef", 4, -4)."<br>"; // devuelve false
			echo substr("abcdef", -3, -1); // devuelve "de"

			//2. strstr: Encuentra la primera aparición de una cadena
			//Descripción y sintaxis:  strstr ( string $haystack , mixed $needle [, bool $before_needle = FALSE ] ) : string
			echo "<h4>2. strstr</h4>";
			$email  = 'name@example.com';
			$domain = strstr($email, '@');
			echo $domain."<br>"; // prints @example.com
			$user = strstr($email, '@', true); // As of PHP 5.3.0
			echo $user; // prints name

			//3. strpos: Encuentra la posición de la primera aparición de una subcadena en una cadena
			//Descripción y sintaxis:  strpos ( string $haystack , mixed $needle [, int $offset = 0 ] ) : int
			echo "<h4>3. strpos</h4>";
			$cadena = 'abc';
			$subcadena   = 'a';
			$pos = strpos($cadena, $subcadena);
			if ($pos === false)
				echo "La cadena '$subcadena' no se encontró en la cadena '$cadena'";
			else
				echo "La cadena '$subcadena' se encontró en la cadena '$cadena' y existe en la posición $pos";

			//4. implode: Unir elementos de una matriz con una cadena
			/*Descripción y sintaxis:
				implode ( string $glue , array $pieces ) : string
				implode ( array $pieces ) : string*/
			echo "<h4>4. implode</h4>";
			$array = array('lastname', 'email', 'phone');
			$comma_separated = implode(",", $array);
			echo $comma_separated."<br>"; // lastname,email,phonex
			// Cadena vacía cuando se usa una matriz vacía:
			var_dump(implode('hello', array())); // string(0) ""

			//5. explode: Dividir una cadena por una cadena
			//Descripción y sintaxis:  explode ( string $delimiter , string $string [, int $limit = PHP_INT_MAX ] ) : array
			echo "<h4>5. explode</h4>";
			// Example 1
			$pizza  = "piece1 piece2 piece3 piece4 piece5 piece6";
			$pieces = explode(" ", $pizza);
			echo $pieces[0]."<br>"; // piece1
			echo $pieces[1]."<br>"; // piece2
			// Example 2
			$data = "foo:*:1023:1000::/home/foo:/bin/sh";
			list($user, $pass, $uid, $gid, $gecos, $home, $shell) = explode(":", $data);
			echo $user."<br>"; // foo
			echo $pass."<br>"; // *
			/* Una cadena que no contiene el delimitador simplemente
				devuelve una matriz de una longitud de la cadena original.*/
			$input1 = "hello";
			$input2 = "hello,there";
			$input3 = ',';
			echo var_dump( explode( ',', $input1 ))."<br>"; //array(1) { [0]=> string(5) "hello" }
			echo var_dump( explode( ',', $input2 ))."<br>"; //array(2) { [0]=> string(5) "hello" [1]=> string(5) "there" }
			echo var_dump( explode( ',', $input3 )); //array(2) { [0]=> string(0) "" [1]=> string(0) "" }

			//6. utf8_encode: Codifica una cadena ISO-8859-1 a UTF-8
			//Descripción y sintaxis:  utf8_encode ( string $data ) : string
			echo "<h4>6. utf8_encode</h4>";
			echo utf8_encode("|1234567890'	qwertyuiop´+asdfghjklñ{}<zxcvbnm,.-°!\"#$%&/()=?¡QWERTYUIOP¨*ASDFGHJKLÑ[]>ZXCVBNM;:_"."<br>");
			echo "|1234567890'	qwertyuiop´+asdfghjklñ{}<zxcvbnm,.-°!\"#$%&/()=?¡QWERTYUIOP¨*ASDFGHJKLÑ[]>ZXCVBNM;:_";

			//7. utf8_decode: Convierte una cadena con caracteres ISO-8859-1 codificados con UTF-8 a un solo byte ISO-8859-1
			//Descripción y sintaxis:  utf8_decode ( string $data ) : string
			echo "<h4>7. utf8_decode</h4>";
			echo utf8_decode("|1234567890'	qwertyuiop´+asdfghjklñ{}<zxcvbnm,.-°!\"#$%&/()=?¡QWERTYUIOP¨*ASDFGHJKLÑ[]>ZXCVBNM;:_"."<br>");
			echo "|1234567890'	qwertyuiop´+asdfghjklñ{}<zxcvbnm,.-°!\"#$%&/()=?¡QWERTYUIOP¨*ASDFGHJKLÑ[]>ZXCVBNM;:_";

			//8. array_pop: Saca el elemento final de la matriz
			//Descripción y sintaxis:  array_pop ( array &$array ) : mixed
			echo "<h4>8. array_pop</h4>";
			$stack = array("orange", "banana", "apple", "raspberry");
			$fruit = array_pop($stack);
			print_r($stack); //Array ( [0] => orange [1] => banana [2] => apple )

			//9. array_push: Agrega uno o más elementos al final de la matriz
			//Descripción y sintaxis:  array_push ( array &$array [, mixed $... ] ) : int
			echo "<h4>9. array_push</h4>";
			$stack = array("orange", "banana");
			array_push($stack, "apple", "raspberry");
			print_r($stack); //Array ( [0] => orange [1] => banana [2] => apple [3] => raspberry ) 

			//10. array_diff: Calcula la diferencia de matrices
			//Descripción y sintaxis:  array_diff ( array $array1 , array $array2 [, array $... ] ) : array
			echo "<h4>10. array_diff</h4>";
			$array1 = array("a" => "green", "red", "blue", "red");
			$array2 = array("b" => "green", "yellow", "red");
			$result = array_diff($array1, $array2);
			print_r($result); //Array ( [1] => blue )

			//11. array_walk: Aplicar una función proporcionada por el usuario a cada miembro de una matriz
			//Descripción y sintaxis:  array_walk ( array &$array , callable $callback [, mixed $userdata = NULL ] ) : bool
			echo "<h4>11. array_walk</h4>";
			$fruits = array("d" => "limon", "a" => "naranja", "b" => "platano", "c" => "manzana");
			function test_alter(&$item1, $key, $prefix) {
				$item1 = "$prefix: $item1";
			}
			function test_print($item2, $key) {
				echo "$key. $item2<br>";
			}
			echo "Antes de ...:<br>";
			array_walk($fruits, 'test_print');
			array_walk($fruits, 'test_alter', 'fruta');
			echo "... y después:<br>";
			array_walk($fruits, 'test_print');

			//12. sort: Ordenar una matriz
			//Descripción y sintaxis:  sort ( array &$array [, int $sort_flags = SORT_REGULAR ] ) : bool
			echo "<h4>12. sort</h4>";
			$fruits = array("banana", "lemon", "apple", "orange");
			echo "Antes de ser ordenado:"."<br>";
			foreach ($fruits as $key => $val) {
				echo "fruits[".$key."] = ".$val."<br>";
			}
			sort($fruits);
			echo "Despues de ser ordenado:"."<br>";
			foreach ($fruits as $key => $val) {
				echo "fruits[".$key."] = ".$val."<br>";
			}

			//13. current: Devuelve el elemento actual en una matriz
			//Descripción y sintaxis:  current ( array $array ) : mixed
			echo "<h4>13. current</h4>";
			$transport = array('foot', 'bike', 'car', 'plane');
			echo current($transport)."<br>"; // 'foot';
			echo next($transport)."<br>";    // 'bike';
			echo current($transport)."<br>"; // 'bike';
			echo prev($transport)."<br>";    // 'foot';
			echo end($transport)."<br>";     // 'plane';
			echo current($transport)."<br>"; // 'plane';
			$arr = array();
			echo var_dump(current($arr))."<br>"; // bool(false)
			$arr = array(array());
			echo var_dump(current($arr)); // array(0) { }

			//14. date: Formatear una hora / fecha local
			//Descripción y sintaxis:  date ( string $format [, int $timestamp = time() ] ) : string
			//https://www.php.net/manual/en/function.date.php
			echo "<h4>14. date</h4>";
			// set the default timezone to use. Available since PHP 5.1
			date_default_timezone_set('UTC');
			// Prints something like: Monday
			echo date("l")."<br>";
			// Prints something like: Monday 8th of August 2005 03:12:46 PM
			echo date('l jS \of F Y h:i:s A')."<br>";
			// Prints: July 1, 2000 is on a Saturday
			echo "July 1, 2000 is on a " . date("l", mktime(0, 0, 0, 7, 1, 2000))."<br>";
			/* use the constants in the format parameter */
			// prints something like: Wed, 25 Sep 2013 15:28:57 -0700
			echo date(DATE_RFC2822)."<br>";
			// prints something like: 2000-07-01T00:00:00+00:00
			echo date(DATE_ATOM, mktime(0, 0, 0, 7, 1, 2000))."<br>";
			// prints something like: Wednesday the 15th
			echo date('l \t\h\e jS')."<br>";
			echo mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"))."<br>"; //tomorrow
			echo mktime(0, 0, 0, date("m")-1, date("d"),   date("Y"))."<br>"; //lastmonth
			echo mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1)."<br>"; //nextyear

			//15. empty: Determinar si una variable está vacía
			//Descripción y sintaxis:  empty ( mixed $var ) : bool
			echo "<h4>15. empty</h4>";
			$var = 0;
			// Evalúa a verdadero porque $var está vacío
			if (empty($var)) {
				echo '$var es 0, está vacío o no está configurado en absoluto<br>';
			}
			// Evalúa como verdadero porque $var está configurado
			if (isset($var)) {
				echo '$var está configurado aunque esté vacío<br>';
			}

			//16. isset: Determine si una variable se declara y es diferente de NULL
			//Descripción y sintaxis:  isset ( mixed $var [, mixed $... ] ) : bool
			echo "<h4>16. isset</h4>";
			$var = '';
			// This will evaluate to TRUE so the text will be printed.
			if (isset($var)) {
				echo "This var is set so I will print.<br>";
			}
			// In the next examples we'll use var_dump to output
			// the return value of isset().
			$a = "test";
			$b = "anothertest";
			echo var_dump(isset($a))."<br>";      // TRUE
			echo var_dump(isset($a, $b))."<br>"; // TRUE
			unset ($a);
			echo var_dump(isset($a))."<br>";     // FALSE
			echo var_dump(isset($a, $b))."<br>"; // FALSE
			$foo = NULL;
			var_dump(isset($foo));   // FALSE

			//17. serialize: Generates a storable representation of a value
			//Descripción y sintaxis:  serialize ( mixed $value ) : string
			//https://www.php.net/manual/en/function.serialize.php
			echo "<h4>17. serialize</h4>";
			// $session_data contiene una matriz multidimensional con la sesión
			// información para el usuario actual. Utilizamos serialize () para almacenar
			// en una base de datos al final de la solicitud.
			$conn = odbc_connect("webdb", "php", "chicken");
			$stmt = odbc_prepare($conn, "UPDATE sessions SET data = ? WHERE id = ?");
			$sqldata = array (serialize($session_data), $_SERVER['PHP_AUTH_USER']);
			if (!odbc_execute($stmt, $sqldata)) {
				$stmt = odbc_prepare($conn,
				"INSERT INTO sessions (id, data) VALUES(?, ?)");
				if (!odbc_execute($stmt, $sqldata)) {
					/* Something went wrong.. */
				}
			}

			//18. unserialize: Crea un valor PHP a partir de una representación almacenada
			//Descripción y sintaxis:  unserialize ( string $str [, array $options ] ) : mixed
			//https://www.php.net/manual/en/function.unserialize.php
			echo "<h4>18. unserialize</h4>";
			// Here, we use unserialize() to load session data to the
			// $session_data array from the string selected from a database.
			// This example complements the one described with serialize().
			$conn = odbc_connect("webdb", "php", "chicken");
			$stmt = odbc_prepare($conn, "SELECT data FROM sessions WHERE id = ?");
			$sqldata = array($_SERVER['PHP_AUTH_USER']);
			if (!odbc_execute($stmt, $sqldata) || !odbc_fetch_into($stmt, $tmp)) {
				// if the execute or fetch fails, initialize to empty array
				$session_data = array();
			} else {
				// we should now have the serialized data in $tmp[0].
				$session_data = unserialize($tmp[0]);
				if (!is_array($session_data)) {
					// something went wrong, initialize to empty array
					$session_data = array();
				}
			}
			//La tarea debe estar en un archivo php debidamente comentada con una breve explicacion de cada función, recuerda subir tu tarea en tu repositorio
		?>
	</body>
</html>
