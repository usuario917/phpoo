<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>formulario.php</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
		<?php
			//https://www.aprenderaprogramar.com/index.php?option=com_content&view=article&id=612:php-consultas-mysql-mysqliconnect-selectdb-query-fetcharray-freeresult-close-ejemplos-cu00841b&catid=70&Itemid=193
			/*$link = mysqli_connect("rocoelwuero", "root", "michingaderanoesparamysql");
			mysqli_select_db($link, "cursoSQL");
			$tildes = $link->query("SET NAMES 'utf8'"); //Para que se muestren las tildes
			$query="INSERT INTO alumno VALUES('308436047', 'Mariana', 'Aguirre', 'Perez','M', '1981-02-15',NULL)";
			$result = mysqli_query($link, $query);
			//$result = mysqli_query($link, "SELECT * FROM agenda");
			/*mysqli_data_seek ($result, 0);
			$extraido= mysqli_fetch_array($result);
			echo "- Nombre: ".$extraido['nombre']."<br/>";
			echo "- Apellidos: ".$extraido['apellidos']."<br/>";
			echo "- Dirección: ".$extraido['direccion']."<br/>";
			echo "- Teléfono: ".$extraido['telefono']."<br/>";
			echo "- Edad: ".$extraido['edad']."<br/>";* /
			mysqli_free_result($result);
			mysqli_close($link);
			if(
				isset($_POST["numCuenta"]) && 
				!empty($_POST["numCuenta"]) && 
				isset($_POST["nombre"]) && 
				!empty($_POST["nombre"]) && 
				isset($_POST["primerApe"]) && 
				!empty($_POST["primerApe"]) && 
				isset($_POST["segundoApe"]) && 
				!empty($_POST["segundoApe"]) && 
				isset($_POST["pass"]) && 
				!empty($_POST["pass"]) && 
				isset($_POST["genero"]) && 
				!empty($_POST["genero"]) && 
				isset($_POST["fechaNacimiento"]) && 
				!empty($_POST["fechaNacimiento"])
			) {
			}*/

			//Enlazado con el atributo "name", y a su ves esta con su valor asignado
			if(isset($_POST["numCuenta"]) && !empty($_POST["numCuenta"])) {
				echo htmlspecialchars($_POST["numCuenta"]);
			}
			if(isset($_POST["nombre"]) && !empty($_POST["nombre"])) {
				echo htmlspecialchars($_POST["nombre"]);
			}
			if(isset($_POST["primerApe"]) && !empty($_POST["primerApe"])) {
				echo htmlspecialchars($_POST["primerApe"]);
			}
			if(isset($_POST["segundoApe"]) && !empty($_POST["segundoApe"])) {
				echo htmlspecialchars($_POST["segundoApe"]);
			}
			if(isset($_POST["pass"]) && !empty($_POST["pass"])) {
				echo htmlspecialchars($_POST["pass"]);
			}
			if(isset($_POST["genero"]) && !empty($_POST["genero"])) {
				echo htmlspecialchars($_POST["genero"]);
			}
			if(isset($_POST["fechaNacimiento"]) && !empty($_POST["fechaNacimiento"])) {
				echo htmlspecialchars($_POST["fechaNacimiento"]);
			}
			/*Dicho formulario contendrá los datos de la tabla alumno obtenida de la base de datos de su curso de análisis de base de datos el script esta de nuevo disponible abajo:
			1. numero de cuenta (integer)		En realidad debe de ser string, asi esta creada la BD
			2. nombre (string)
			3. Primer apellido (string)
			4. Segundo apellido (string)
			5. Genero (char) 
			5.1. M, F u otro
			6. Fecha de nacimiento (date) 
			7. y adicionalmente la contraseña*/
		?>
		<script type="text/javascript">
			$numCuenta=1;
			$nombre=1;
			$primerApe=1;
			$segundoApe=1;
			$pass=1;
			$genero=1;
			$fechaNacimiento=1;
			let validarRegexs = () => {
				//123456789
				//314024576
				if(new RegExp("^([0-9]){9}$").test(document.getElementById("numCuenta").value)) {
					alert("Numero de cuenta valida");
					$numCuenta=0;
				}
				else {alert("Error en el numero de cuenta");}

				//Oscar
				if(new RegExp("^([a-zA-Z])*([a-zA-Z\ ])$").test(document.getElementById("nombre").value)) {
					alert("Nombre valido");
					$nombre=0;
				}
				else {alert("Error en el nombre");}

				//Rojas
				if(new RegExp("^([a-zA-Z])*([a-zA-Z\ ])$").test(document.getElementById("primerApe").value)) {
					alert("Primer apellido valido");
					$primerApe=0;
				}
				else {alert("Error en el primer apellido");}

				//Castillo
				if(new RegExp("^([a-zA-Z])*([a-zA-Z\ ])$").test(document.getElementById("segundoApe").value)) {
					alert("Segundo apellido valido");
					$segundoApe=0;
				}
				else {alert("Error en el segundo apellido");}

				//123456789123456789
				//$v&_j*&l110qa6
				if(new RegExp("^([a-zA-Z0-9]){8,20}$").test(document.getElementById("pass").value)) {
					alert("Password valido");
					$pass=0;
				}
				else {alert("Error en el password");}

				//'M' o 'F' o 'O'
				if(document.querySelector('input[name="genero"]:checked')) {
					var arrGeneros=document.getElementsByName("genero");
					for (let i=0; i<arrGeneros.length; i++) {
						if(arrGeneros[i].checked){
							if(new RegExp("^M|F|O$").test(elementGenero=arrGeneros[i].value)) {
								alert("Genero valido");
								$genero=0;
							}
						}
					}
				}
				else {alert("Error en el genero");}
				//alert(document.getElementsByName("genero"));
				//alert(document.querySelector('input[name="genero"]:checked'));

				//var textFechaNacimiento = document.getElementById("fechaNacimiento").value;
				//var regex = new RegExp("^([0-9]){2}/([0-9]){2}/([0-9]){4}$");
				//if(regex.test(textFechaNacimiento)) {
				//if(regex.test(document.getElementById("fechaNacimiento").value)) {
				//05/09/1997
				if(new RegExp("^([0-9]){2}/([0-9]){2}/([0-9]){4}$").test(document.getElementById("fechaNacimiento").value)) {
					alert("Fecha de nacimiento valida");
					$fechaNacimiento=0;
				}
				else {alert("Error en la fecha de nacimiento");}

				if($numCuenta==1 && $nombre==1 && $primerApe==1 && $segundoApe==1 && $pass==1 && $genero==1 && $fechaNacimiento==1) {alert("Todos los campos estan mal");}
			}
		</script>
	</head>
	<body>
		<h1>Formulario</h1>
		<form action="formulario.php" method="POST" enctype="multipart/form-data">
			Número de cuenta: <input id="numCuenta" class="textos" name="numCuenta" type="text"><br><br>
			Nombre: <input id="nombre" class="textos" name="nombre" type="text"><br><br>
			Primer apellido: <input id="primerApe" class="textos" name="primerApe" type="text"><br><br>
			Segundo apellido: <input id="segundoApe" class="textos" name="segundoApe" type="text"><br><br>
			Contraseña: <input id="pass" class="textos" name="pass" type="password"><br><br>
			Género: 
				<input name="genero" type="radio" value="M">M<br>
				<input name="genero" type="radio" value="F">F<br>
				<input name="genero" type="radio" value="O">Otro<br><br>
			Fecha de nacimiento: <input id="fechaNacimiento" class=textos name="fechaNacimiento" type="text" placeholder="dd/mm/aaaa"><br><br>
			<!--<input id="envio" type="submit" value="Enviar xD" onclick="validarRegexs()">-->
			<button id="envio" type="submit" onclick="validarRegexs()">Enviar xD</button>
		</form>
		<?php
		?>
	</body>
</html>
