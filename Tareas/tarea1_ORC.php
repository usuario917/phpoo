<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>tarea1_ORC.php</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" media="screen" href="main.css" />
		<script src="main.js"></script>
	</head>
	<body>
		<?php
			/*
				Crear un script que genere lo que se muestra en las siguientes imágenes considerando lo siguiente:
				1. Utilizar una variable para el máximo de filas que queremos (30 filas). en la imagen se muestran solo 10 filas
				2. Debe estar centrado.
				3. Pueden ocupar cualquier simbolo, imagen (en la imagen se ocupo Font Awesome)
				4. No se repite longitud en las filas.
				5. El archivo se nombrara como "tarea1_iniciales_de_su_nombre.php" (ejemplo tarea1_HGCM.php)
			*/
			for ($maxFilas=30; $maxFilas>0; $maxFilas--) { //Piramide
				echo "<center>";
				for ($i=$maxFilas; $i<30; $i++) {
					echo "*";
				}
				echo "</center>";
			}
			for ($maxFilas=30; $maxFilas>0; $maxFilas--) {//Piramide invertida
				echo "<center>";
				for ($i=$maxFilas; $i>0; $i--) {
					echo "*";
				}
				echo "</center>";
			}
		?>
	</body>
</html>
