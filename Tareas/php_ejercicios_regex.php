<?php
	/*
		Rojas Castillo Oscar
	*/
	//Realizar una expresión regular que detecte emails correctos.
	$patron="/^[a-z|_|\\-|0-9]{1,}@[a-z|_|-|0-9]{1,}(.)com$/i";
	echo preg_match($patron, "usuario917@gmail.com")."<br>";
	//Realizar una expresion regular que detecte Curps Correctos: ABCD123456EFGHIJ78.
	$patron="/^[A-Z]{4}[0-9]{6}[A-Z]{6}[0-9]{2}$/i";
	echo preg_match($patron, "ABCD123456EFGHIJ78")."<br>";
	//Realizar una expresion regular que detecte palabras de longitud mayor a 50 formadas solo por letras.
	$patron="/^[A-Z]{51,}$/i";
	echo preg_match($patron, "qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq")."<br>";//qqqqqqqqqq	10
	//Crea una funcion para escapar los simbolos especiales.
	$claves = '$40 * ? [a-z]{4} g3/400';
	$claves = preg_quote($claves, '/');
	echo $claves."<br>"; // devuelve \$40 \* \? \[a\-z\]\{4\} g3\/400
	//Crear una expresion regular para detectar números decimales.
	$patron="/^1[0-9]$/i";
	echo preg_match($patron, "19")."<br>";
?>