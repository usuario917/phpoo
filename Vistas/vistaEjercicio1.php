<!DOCTYPE html>
<html>
	<head>
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/bootstrap-grid.css">
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
		<title>Indice</title>
		<?php require('../Clases/ejercicio1/Carro.php'); ?>
		<?php require('../Clases/ejercicio1/Moto.php'); ?>
	</head>
	<body>
		<!-- aqui puedes insertar el mesaje del servidor para Moto-->
		<div class="container" style="margin-top: 4em">
			<header><h1>Carro y Moto</h1></header><br>
			<form method="post" action="vistaEjercicio1.php">
			<div class="col-sm-4">
					<label>Color del carro: <input class="form-control" type="color" id="colorCarro" name="colorCarro">
					<label>Tipo de carro: <input class="form-control" type="text" id="tipoCarro" name="tipoCarro" />
				</div>
				<button class="btn btn-primary" type="submit" >Enviar datos carro</button>
			</form>
			<br>
			<br>
			<form method="post" action="vistaEjercicio1.php">
			<div class="col-sm-4">
					<label>Color de la moto: <input class="form-control" type="color" id="colorCarro" name="colorMoto">
					<label>Tipo de moto: <input class="form-control" type="text" id="tipoCarro" name="tipoMoto" />
				</div>
				<button class="btn btn-primary" type="submit" >Enviar datos moto</button>
			</form>
			<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
		</div>
		<?php
			echo "<div>".$mensajeServidor."</div>";
		?>
	</body>
</html>
