<?php
	//Tu tarea será la creación de otro archivo.php en donde crees una clase con dichos métodos (__construct y __destruct)
	//pero ahora que al momento de instanciar a la clase te genere una contraseña solo 4 letras mayúsculas y al momento de
	//destruir el objeto se muestre en pantalla tu contraseña.
	class Contrasena {
		private $datoPassword;
		private $password=null;
		private $letrasMayus;
		public function __construct() {
			$this->datoPassword = "";
			$this->letrasMayus = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
			str_shuffle($this->letrasMayus); //str_shuffle: Reordena aleatoriamente una cadena
			for( $i=1; $i<=4; $i++) {
				$this->datoPassword[$i] = $this->letrasMayus[rand(0,strlen($this->letrasMayus))];
				str_shuffle($this->letrasMayus);
			}
			foreach ($this->datoPassword as $passwordGenerado) {
				$this->password.=$passwordGenerado;
			}
		}
		public function __destruct(){
			echo 'El objeto ha sido destruido';
			return 'Hola, este es tu contraseña: '.$this->password;
		}
	}
	$mensaje='';
	if (!empty($_POST)){
		$pass= new Contrasena();
		$mensaje=$pass->__destruct();
	}
?>