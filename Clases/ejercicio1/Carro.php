<?php
	class Carro {
		//declaracion de propiedades
		public $color;
		public $tipo;
	}
	//crea aqui la clase Moto junto con dos propiedades public
	//inicializamos el mensaje que lanzara el servidor con vacio
	$mensajeServidor='';
	//crea aqui la instancia o el objeto de la clase Moto
	$carro1 = new Carro;
	if (!empty($_POST['colorCarro']) && !empty($_POST['tipoCarro'])) {
		//almacenamos el valor mandado por POST en el atributo
		// recibe aqui los valores mandados por post 
		$carro1->color=$_POST['colorCarro'];
		$carro1->tipo=$_POST['tipoCarro'];
		//se construye el mensaje que sera lanzado por el servidor
		$mensajeServidor='<h4>Carro</h4><ol>'
			.'<li>Carro->Color: '.$carro1->color.'</li>'
			.'<li>Carro->Tipo: '.$carro1->tipo.'</li>'
		.'</ol>';
		?>
			<div><?php echo $mensajeServidor; ?></div>
		<?php
	}
?>