<?php
	//crea aqui la clase Moto junto con dos propiedades public
	class Moto {
		//declaracion de propiedades
		public $color;
		public $tipo;
	}
	//inicializamos el mensaje que lanzara el servidor con vacio
	$mensajeServidor='';
	//crea aqui la instancia o el objeto de la clase Moto
	$moto1 = new Moto;
	if (!empty($_POST['colorMoto']) && !empty($_POST['tipoMoto'])) {
		//almacenamos el valor mandado por POST en el atributo
		// recibe aqui los valores mandados por post 
		$moto1->color=$_POST['colorMoto'];
		$moto1->tipo=$_POST['tipoMoto'];
		//se construye el mensaje que sera lanzado por el servidor
		$mensajeServidor='<h4>Moto</h4><ol>'
			.'<li>Moto->Color: '.$moto1->color.'</li>'
			.'<li>Moto->Tipo: '.$moto1->tipo.'</li>'
		.'</ol>';
		?>
			<div><?php echo $mensajeServidor; ?></div>
		<?php
	}
?>