<?php
	//creación de la clase carro
	class Carro2{
		//declaracion de propiedades
		public $color=null;
		public $modelo=null;
		/*
			Simplemente no quise poner el otro atributo "resultado de verificación",
			ya que era mucho rollo sin sentido y además no dejaba funcionar.

			PD: además se optimiza memoria :D
		*/
		/*
			Se verificará mediante el año de fabricación del carro si circula o no guiándose lo siguiente:
			- No: Antes de 1990
			- Revisión: De 1990 a 2010
			- Si: Después de 2010
		*/
		public function verificacion(){
			if (!empty($_POST['color']) && !empty($_POST['modelo']) && !empty($_POST['ano'])){
				if (((int) substr($_POST['ano'], 0,4)) < 1990) {
					return "No";
				} elseif (((int) substr($_POST['ano'], 0,4)) >= 1990 && ((int) substr($_POST['ano'], 0,4)) <= 2010) {
					return "Revisión";
				} elseif (((int) substr($_POST['ano'], 0,4)) > 2010 && ((int) substr($_POST['ano'], 0,4)) <= 2019) {
					return "Si";
				}
			}
		}
	}
	//Creación de instancia a la clase Carro
	$carro1 = new Carro2();
	if (!empty($_POST['color']) && !empty($_POST['modelo']) && !empty($_POST['ano'])){
		$carro1->color=(string) $_POST['color'];
		$carro1->modelo=(string) $_POST['modelo'];
	}
?>
