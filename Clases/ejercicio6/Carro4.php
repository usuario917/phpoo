<?php
	//En el fichero Clases/ejercicio6 verás dos archivos uno llamado transporte.php y otro Carro4.php verás que en el caso
	//de este último archivo, las clases que existen en él, extienden de transporte y contiene una inclusión del archivo
	//transporte.php, eso significa que todas las clases en Carro4.php heredan la clase del archivo tranporte.php.

	//Tu tarea será separar las clases de Carro4.php en archivos únicos, es decir, carro.php, avion.php y barco.php con sus
	//respectivas clases
	//Y ADEMAS crear otra clase (un transporte nuevo) que puede heredar las funcionalidades de transporte
	//es decir nuevas subclases de transporte.

	//Finalmente dentro de la vista Vista/vistaEjercicio6 incluir el nuevo transporte para que se muestre en el formulario
	include_once('Transporte.php');
	include_once('Avion.php');
	include_once('Barco.php');
	include_once('Carro.php');
	$mensaje='';
	if (!empty($_POST['tipo_transporte'])) {
		//declaracion de un operador switch
		switch ($_POST['tipo_transporte']) {
			case 'aereo':
				//creacion del objeto con sus respectivos parametros para el constructor
				$jet1= new Avion('jet','400','gasoleo','2');
				$mensaje=$jet1->resumenAvion();
				break;
			case 'terrestre':
				$carro1= new Carro('carro','200','gasolina','4');
				$mensaje=$carro1->resumenCarro();
				break;
			case 'maritimo':
				$bergantin1= new Barco('bergantin','40','na','15');
				$mensaje=$bergantin1->resumenBarco();
				break;
		}
	}
?>
